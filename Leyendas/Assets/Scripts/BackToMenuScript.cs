using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenuScript : MonoBehaviour
{
    public bool cabezaOK = false;
    public bool ojosOK = false;
    public bool bocaOK = false;
    public bool ropaOK = false;

    public bool correct = false;

    public GameObject message, MainMenuButton;


    public void BTM()
    {        
            SceneManager.LoadScene("Menu");                   
    }
    public void CheckSolution()
    {
        if (cabezaOK && ojosOK && bocaOK && ropaOK)
        {
            message.SetActive(true);
            AudioManager.Instance.PlaySFX("Acierto");
            correct = true;
            MainMenuButton.SetActive(true);
        }
        else
        {
            correct = false;
            AudioManager.Instance.PlaySFX("Error");
        }
    }
}

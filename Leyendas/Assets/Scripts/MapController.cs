using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    //REFERENCIAR LOS SLOTS AL SCRIPT, LEER LOS BOOLEANOS Y SI TODOS TRUE MENSAJE VICTORIA
    #region Referencias
    public ImageSlot imageslot;
    public ImageSlot imageslot2;
    public ImageSlot imageslot3;
    public ImageSlot imageslot4;
    public bool InPause;
    public GameObject interfazVictoria;
    public GameObject interfazPausa;
    public GameObject ExitButton;
    public GameObject initialUI;
    public GameObject GuessButton;
    public Mapa_icono player;
    
    #endregion

    private void Start()
    {
        
        GuessButton.SetActive(false);
        ExitButton.SetActive(false);
        interfazPausa.SetActive(false);
        interfazVictoria.SetActive(false);
        InPause = false;
    }

    // Update is called once per frame
    void Update()
    {
      if (imageslot.correcto == true && imageslot2.correcto == true && imageslot3.correcto == true && imageslot4.correcto == true)
      {
            Victory();
      }
        if (player.BusFound==true && player.HouseFound==true && player.alleyFound==true && player.CarFound==true)
        {
            GuessButton.SetActive(true);
        }
    }
    

    void Victory()
    {
        interfazPausa.SetActive(false);
        Time.timeScale = 0;
        interfazVictoria.SetActive(true);
    }
    public void Pause()
    {
        InPause = true;
        interfazPausa.SetActive(true);
        ExitButton.SetActive(false);
        Time.timeScale = 0;
    }
    public void Unpause()
    {
        Time.timeScale = 1;
        InPause = false;
        interfazPausa.SetActive(false);
        ExitButton.SetActive(true);
    }
    public void DestroyInitialUI()
    {
        Destroy(initialUI);
        ExitButton.SetActive(true);        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mapa_icono : MonoBehaviour
{
    public MapController controlador;
    public GameObject spotFound;
    public bool BusFound;
   public bool alleyFound;
   public bool CarFound;
   public bool HouseFound;
    CanvasGroup cv;
    public RectTransform rT;
    private void Start()
    {
     
        BusFound = false;
        alleyFound = false;
        CarFound = false;
        HouseFound = false;
        cv = spotFound.GetComponent<CanvasGroup>();
        cv.alpha = 0;
        spotFound.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        rT.anchoredPosition = RectTransformUtility.WorldToScreenPoint(Camera.main, transform.position - new Vector3(0.6f,0.3f,0f));
        if (Input.GetMouseButtonDown(0) && controlador.InPause == false)
        {
            Vector3 CursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(CursorPos.x , CursorPos.y, -2);
        }
        if (cv.alpha > 0)
        {
            cv.alpha -= Time.deltaTime;
        }        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BusStop" && BusFound == false)
        {
            BusFound = true;
            Debug.Log(BusFound);
            cv.alpha = 1;
            AudioManager.Instance.PlaySFX("Acierto");
        }

        if (collision.gameObject.tag == "Alley" && alleyFound == false)
        {
            alleyFound = true;
            Debug.Log(alleyFound);
            cv.alpha = 1;
            AudioManager.Instance.PlaySFX("Acierto");
        }

        if (collision.gameObject.tag == "House" && HouseFound == false)
        {
            HouseFound = true;
            Debug.Log(HouseFound);
            cv.alpha = 1;
            AudioManager.Instance.PlaySFX("Acierto");
        }

        if (collision.gameObject.tag == "Car" && CarFound == false)
        {
            CarFound = true;
            Debug.Log(CarFound);
            cv.alpha = 1;
            AudioManager.Instance.PlaySFX("Acierto");
        }
    }
  
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ImageSlot : MonoBehaviour, IDropHandler
{
    public Image foto;

    public bool correcto;

    private void Start()
    {
        correcto = false;
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");
        if (eventData.pointerDrag != null)
        {
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            foto = eventData.pointerDrag.GetComponent<Image>();
            if (gameObject.tag == foto.tag)
            {
                correcto = true;
                Debug.Log(correcto);
            }
            else
            {
                correcto = false;
            }
        }
        else
        {
            foto = null;
            correcto = false;            
        }             
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance; //singleton 
    public Sound[] sfx; //array sonidos
    AudioSource sfxSource;
    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        sfxSource = GetComponent<AudioSource>(); //asignamos audio source del propio audio manager
        PlaySFX("Intro");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1))
        {
             PlaySFX("MouseClick"); //sonido del click del raton
        }      
    }

    public void PlaySFX(string name)
    {
        Sound s = Array.Find(sfx, x=> x._name == name);

        if (s == null)
        {
            Debug.Log("efecto de sonido no encontrado, comprueba que has escrito bien el nombre y que el Sound est� creado en el Audio Manager");
        }
        else
        {
           
            sfxSource.PlayOneShot(s.sound);
        }
    }   
}

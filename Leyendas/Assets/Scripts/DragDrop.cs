using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    #region Componentes
    RectTransform rectTransform;
    CanvasGroup canvasG;
    [SerializeField] Canvas interfaz;
    #endregion

    private void Awake()
    {
        rectTransform = GetComponent < RectTransform>();
        canvasG = GetComponent<CanvasGroup>();
    }

    #region Drag & Drop
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
        canvasG.blocksRaycasts = false; //cuando se empieza a dragear los raycast dejan de detectar la imagen
        canvasG.alpha = 0.6f; // la imagen se transparenta levemente
    }

    public void OnDrag (PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        rectTransform.anchoredPosition += eventData.delta / interfaz.scaleFactor;   //el icono se mueve siguiendo al raton
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        canvasG.alpha = 1f; // la imagen vuelve al 100% de opacidad
        canvasG.blocksRaycasts = true; //cuando se termina de dragear los raycast vuelven a detectar la imagen       
    }

    public void OnPointerDown (PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }
      #endregion
}
